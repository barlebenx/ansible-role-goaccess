ansible-role-goaccess
=========

Install goaccess package.

Requirements
------------


Role Variables
--------------



Dependencies
------------



Example Playbook
----------------


```yaml
- hosts: servers
  roles:
      - role: ansible-role-goaccess
```

License
-------

MIT

Author Information
------------------

